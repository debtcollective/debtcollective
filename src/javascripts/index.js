require('./shared');

require('./_entries/sessions/new');
require('./_entries/sessions/changePassword');
require('./_entries/sessions/passwordRecover');
require('./_entries/dispute-tools');
require('./_entries/dispute-tools/show');
require('./_entries/disputes/show');
require('./_entries/disputes/showForVisitors');
require('./_entries/home/about');
require('./_entries/home/contact');
require('./_entries/home/tos');
require('./_entries/home/vision');
require('./_entries/home');
require('./_entries/users/edit');
require('./_entries/users/new');
require('./_entries/users/show');

require('../stylesheets/index.css');
