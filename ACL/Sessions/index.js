module.exports = {
  Visitor: [
    [
      'new',
      'create',
      'destroy',
      'showEmailForm',
      'sendResetEmail',
      'showPasswordForm',
      'resetPassword',
      true,
    ],
  ],
};
