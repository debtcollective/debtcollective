module.exports = {
  incomplete: 'Incomplete',
  completed: 'Completed',
  inReview: 'In Review',
  documentsSent: 'Documents Sent',
  secondDispute: '2nd Dispute',
  discharged: 'Discharged',
  contactAdmin: 'Contact Admin',
  userToMail: 'User to Mail',
  userUpdate: 'User Update',
};
